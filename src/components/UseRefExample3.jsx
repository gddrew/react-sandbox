import React from 'react';
import { useState } from 'react';
import Todo from './Todo';

function UseRefExample3() {
  // Create a piece of state called 'Show Todo' and 'Set Todo'
  const [showTodo, setShowTodo] = useState(true);

  // When you click the Toggle Todo button, "Todo" disappears;
  // when you click it again, Todo appears
  return (
    <div>
      {showTodo && <Todo />}
      <button
        className='btn btn-primary'
        onClick={() => setShowTodo(!showTodo)}
      >
        Toggle Todo
      </button>
    </div>
  );
}

export default UseRefExample3;
