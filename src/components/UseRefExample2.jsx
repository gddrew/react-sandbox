import React from 'react';
import { useState, useEffect, useRef } from 'react';

function UseRefExample2() {
  // Whenever any state in the component is updated, it gets re-rendered, so capture state
  const [name, setName] = useState();

  const renders = useRef(1); // set to 1 because when we first load the component, that's a render

  // Create another const to get the previous state
  const prevName = useRef('');

  // Set the render to 1 ahead
  useEffect(() => {
    renders.current = renders.current + 1;
    prevName.current = name; // store previous state
  }, [name]);

  // Display the number of renders
  // Display the previous state before next render
  // Create an input so that when we type something in the component gets re-rendered
  return (
    <div>
      <h1>Renders: {renders.current}</h1>
      <h2>Prev Name State: {prevName.current}</h2>
      <input
        type='text'
        value={name}
        onChange={(e) => setName(e.target.value)}
        className='form-control mb-3'
      />
    </div>
  );
}

export default UseRefExample2;
