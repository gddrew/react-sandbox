import React from 'react';
// import { useState } from 'react';
import useLocalStorage from '../hooks/useLocalStorage';

function CustomHookExample2() {
  // const [task, setTask] = useState(''); // useState will cause whatever is typed in the input to show in the hooks component in the Developer Tools of Chrome
  const [task, setTask] = useLocalStorage('task', ''); // useLocalStorage captures what is typed in the input and appear in local storage in the Developer Tools of Chrome
  const [tasks, setTasks] = useLocalStorage('tasks', []); // useLocalStorage to capture arrays onSubmit
  const onSubmit = (e) => {
    e.preventDefault();

    const taskObj = {
      task,
      completed: false,
      date: new Date().toLocaleDateString(),
    };

    setTasks([...tasks, taskObj]);
  };

  return (
    <>
      <form onSubmit={onSubmit} className='w-50'>
        <div className='mb-3'>
          <label className='form-label'>Task</label>
          <input
            className='form-control'
            type='text'
            value={task}
            onChange={(e) => setTask(e.target.value)}
          />
        </div>
        <button type='submit' className='btn btn-primary'>
          Submit
        </button>
      </form>
      <hr />
      {tasks.map((task) => (
        <h3 key={task.date}>{task.task}</h3>
      ))}
    </>
  );
}

export default CustomHookExample2;
