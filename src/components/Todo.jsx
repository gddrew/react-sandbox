import React from 'react';
import { useState, useEffect, useRef } from 'react';

/* Create a scenario in which you can unmount a component before it can handle the expected reponse from the fetch request, which causes a memory leak. React 18 removed the warning.

To handle, create a reference to the component being mounted (line 14). If you want something to happen when the component is unmounted you return it from the useEffect.
 */

function Todo() {
  const [loading, setLoading] = useState(true);
  const [todo, setTodo] = useState({});

  // Create a reference to the component being mounted
  const isMounted = useRef(true);

  useEffect(() => {
    fetch('https://jsonplaceholder.typicode.com/todos/1')
      .then((res) => res.json())
      .then((data) => {
        setTimeout(() => {
          if (isMounted.current) {
            setTodo(data);
            setLoading(false);
          }
        }, 3000); // Set the response time to three seconds so that you have time to click to unmount the component
      });

    // Runs when component is unmounted (React 18 strict mode causes app to render twice initially)
    return () => {
      isMounted.current = false;
    };
  }, [isMounted]);

  // If loading, display 'Loading', else return response from fetch
  return loading ? <h3>Loading...</h3> : <h1>{todo.title}</h1>;
}

export default Todo;
